#include <iostream>
#include <cstdlib>

using namespace std;

template <typename T>
class Stack {
private:
    struct StackElement {
        T data;
        StackElement* next;
    } *top_element;

public:
    Stack() {
        top_element = nullptr;
    }

    Stack(const Stack<T>& s) {
        top_element = nullptr;
        Stack<T> trash_bin;
        StackElement* cur = s.top_element;
        while (cur != nullptr) {
            trash_bin.push(cur->data);
            cur = cur->next;
        }

        while (!trash_bin.empty()) {
            push(trash_bin.top());
            trash_bin.pop();
        }
    }

    ~Stack() {
        clear();
    }

    const Stack<T>& operator=(const Stack& s) {
        if (this == &s)
            return *this;
        
        clear();
        Stack<T> trash_bin;
        StackElement* cur = s.top_element;
        while (cur != nullptr) {
            trash_bin.push(cur->data);
            cur = cur->next;
        }

        while (!trash_bin.empty()) {
            push(trash_bin.top());
            trash_bin.pop();
        }
        return *this;
    }

    bool empty() {
        return top_element == nullptr;
    }

    void push(T const& x) {
        StackElement* elem = new StackElement;
        elem->data = x;
        elem->next = top_element;

        top_element = elem;
    }

    void pop() {
        if (empty()) {
            cout << "[!] Nope. Won't do that." << endl;
            exit(1);
        }

        StackElement* moriturus = top_element;
        top_element = top_element->next;
        delete moriturus;
    }

    T top() {
        if (empty()) {
            cout << "[!] This trash bin is empty, search on." << endl;
            exit(1);
        }

        return top_element->data;
    }

    void clear() {
        while (!empty())
            pop();
    }

};